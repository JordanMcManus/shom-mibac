/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run MIBAC (Multibeam IMU Boresight Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

/*/-------------------------------------------------------------------------------------/*/
// MILAC (Multibeam IMU Latency Automatic Calibration)
//
// Authors                :  Rabine Keyetieu, Gaël Roué                
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/12/21
//--------------------------------------------------------------------------------------/*/


// ------------------------------------------------------------------->>>>>>>>>>>>>>>> LIBS:
// Libs
#include <set>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <windows.h>
#include "MILAC_Estimation.h"

// namespace
using namespace std;
// ----------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>


// ------------------------------------------------------------------->>>>>>>>>>>>>>>> MAIN:
int main()
{
	cout << "<-------- CIDCO: MILAC (Multibeam IMU Latency Automatic Calibration) -----------> " << endl <<  endl;

	// Probability
	double probability = 0.95;

	// Class instantiation
	MILAC_Estimation LatObject;

	// Enter fake latency
	cout << "Enter Fake latency to change the dataset in s: " << endl;
	double FakeLatency; cin >> FakeLatency; cout << endl;

	//---> Initialzation of parameters
	// Latency
	cout << "Initialize the latency for the adjustment in s: " << endl;
	double dt; cin >> dt; cout << endl;
	// Surface
	double A=0, B=0, C=0, D=0, E=0, F=0;

	// Type of selection
	cout << "Type 1 for automatic selection otherwise Percentage selection will be used: " << endl;
	int ApproachAuto; cin >> ApproachAuto; cout << endl;

	// Series of adjustment
	int EstCounter = 1;
	double ConvZone = 1;
	string NameProject, folderData,  dirString; vector<string> fileNamesLines;
	MatrixXd RawData; VectorXd MeanPos; 
	while (ConvZone > 0.0001)
	{
		// Counter of estimation
		cout << endl << "************************************Estimation--------------------->: " << EstCounter << endl;

		// previous estimate
		double dtOld = dt;

		// MILAC results
		VectorXd ResMILAC = LatObject.MILAC_Results(probability, FakeLatency, ApproachAuto, A, B, C, D, E, F, dt,
													NameProject, folderData, dirString, fileNamesLines,
													MeanPos, RawData, EstCounter);
		// Update
		ConvZone = abs(dt - dtOld);
		// ConvZone = 1e-5;
		EstCounter++;

		// Display information
		cout << "Latency (s): " << dt << endl;
		cout << "Difference of latency (s): " << ConvZone << endl << endl;
	}

	// System pause
	system("pause");
	return 0;
}
// ----------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>