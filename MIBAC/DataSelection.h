/* Copyright Shom (13/05/2020)

This software is a computer program whose purpose is to run MIBAC (Multibeam IMU Boresight Automatic Calibration).

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms. */

#pragma once
/*/-------------------------------------------------------------------------------------/*/
// MIBAC (Multibeam IMU Boresight Automatic Calibration)
//
// Class                  :  .h (DataSelection)
// Authors                :  Rabine Keyetieu, Julian Le Deunf, Gaël Roué                 
// Coordinated by         :  Nicolas Seube  
// Date                   :  2016/08/12
//--------------------------------------------------------------------------------------/*/


// --------------------------------------------------------------------------->>>>>> Librairies:
//using namespace std;
using std::string;
using std::vector;
using Eigen::MatrixXd;
using Eigen::VectorXd;

// define
#define PI 3.14159265358979323846;
#define  BUFSIZE 256

//pragma
# pragma comment (lib, "libmx.lib")
# pragma comment (lib, "libeng.lib")
# pragma comment (lib, "libmex.lib")
# pragma comment (lib, "libmat.lib")
// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>>>


class DataSelection
{
public:
	// --------------------------------------------------------------------------->>>>>> Methods:
	// Constructor
	DataSelection(void);

	// Get current directory
	string current_working_directory();

	// Density
	vector<int> density(double Emin, double Emax, double Nmin, double Nmax, MatrixXd const& D, int Nbpoint);

	// Extract soundings from a defined zone
	vector <int> SoundingsInPatchQuad(double Emin, double Emax, double Nmin, double Nmax, 
		                               MatrixXd const& Data, int i, vector <int> indice);

	// Deming Least Squares
	vector <double> TotalLS_Refinement(double A,double B,double C, VectorXd const& X, VectorXd const& Y, VectorXd const& Z,
	 VectorXd const& sigmaX, VectorXd const& sigmaY, VectorXd const& sigmaZ, double eps);

	// Rotation Matrix ENU convention
	MatrixXd RotationMatrix(double Roll,double Pitch,double Yaw);

	// Inverse Rotation  Matrix ENU convention
	MatrixXd RotationMatrixInv(double Roll, double Pitch, double Yaw);

	// ObservCrtieria function
	vector <double> observCriteria(double Xc,double Yc,VectorXd Zc, VectorXd Xb,VectorXd Yb,VectorXd Zb,
	VectorXd Roll, VectorXd Pitch, VectorXd Yaw, double a_g, double b_g);

	//QuadTree function
	vector<double> QuadTree(double Emin, double Emax, double Nmin, double Nmax, int Nbpoint, MatrixXd const& Data, 
	vector <int> indice, double StatisticThreshold, vector <double> Resultat);

	// Reading of data
	MatrixXd ReadingData_DataSelection(std::vector<int> & indice, string & folderData, 
		                                 string & dirString2nd, string & dirString3rd);

	// Criteria extraction
	MatrixXd CriteriaExtraction(vector<double> const& Result_quadtree, vector <double>& indice_kdtree, MatrixXd const& Data,
		                        vector<int> indice);

	// Save file with defined number of surface elements
	MatrixXd SaveFile(string fichierSave, MatrixXd const& Data, MatrixXd const& Criteria_final, 
			  vector<int> indice, string & FileName);

	// Images using matlab engine
	int DataSelection::ImagesDataSelection(vector<double> Cphi, vector<double> Cthe, vector<double> Cpsi, vector<double> FinalCri,
	                                    vector<double> a_f, vector<double> b_f, vector<double> c_f,
										vector<double> Emin_f, vector<double> Emax_f, vector<double> Nmin_f, vector<double> Nmax_f,
										MatrixXd const& Data, vector<int> indice);

	// Data selection
	MatrixXd DataSelection_Results(MatrixXd const& Data, double PlanThreshold, 
						   vector<int> indice, string fichierSave,
						   string & FileName);
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>



private:
	// --------------------------------------------------------------------------->>>>>> Attributs:
	string m_MIBAC_DataSelection;
	// --------------------------------------------------------------------------->>>>>>>>>>>>>>>>
};